## :beetle: Set up Xdebug for PhpStorm and Laravel Valet on Mac :elephant:

* **Install [xDebug](https://xdebug.org/docs/install) php extension via Homebrew**
```
brew install homebrew/php/<php-version>-xdebug
```

* **:elephant: Verify php.ini installation**
```
php --ini
```

*:warning: By default the Xdebug port is 9000, but that’s what Valet runs on, so you’ll need something different.
How does 9001 sound? Perfect.*


* **Set configuration to ext-xdebug.ini file**
```
[xdebug]
zend_extension="/usr/local/opt/php71-xdebug/xdebug.so"
xdebug.remote_autostart=1
xdebug.default_enable=1
xdebug.remote_port=9001
xdebug.remote_host=127.0.0.1
xdebug.remote_connect_back=1
xdebug.remote_enable=1
xdebug.idekey=PHPSTORM
```

* **Restart PHP**
```
valet restart
```

* **:white_check_mark: Confirm it worked**

*You can confirm it worked by running php -v and not see the Xdebug line at the end here:*

```
PHP 7.1.12 (cli) (built: Dec  2 2017 12:15:25) ( NTS )
Copyright (c) 1997-2017 The PHP Group
Zend Engine v3.1.0, Copyright (c) 1998-2017 Zend Technologies
    with Xdebug v2.5.5, Copyright (c) 2002-2017, by Derick Rethans
```
* **Configure PHPStorm**
 
```
# Open Preferences > Languages & Frameworks > PHP.
# Next to the Interpreter dropdown, hit the ... button, and add a new one in the dialog that appears.
# The executable should be /usr/local/bin/php.
# Hit the little refresh icon and all going well, it should say Debugger: Xdebug.
# Apply/OK out of there.

# Open to Preferences > Languages & Frameworks > PHP > Debug.
# In the Xdebug section, Debug Port should be 9001 (like you set in php.ini earlier)
# Uncheck both Force break ... checkboxes.
```
*:warning: If you don’t uncheck both Force break ... checkboxes, the debugger will stop on Valet’s server.php every time.*


* **Set up the app for debugging**

```
# Open Preferences > Languages & Frameworks > PHP > Servers
# Add a new server. Name it whatever. The name of your project is an idea.
# The host should be your dev domain, like: myproject.dev
# Port should be 80
```

* **Create a debug configuration**

```
# Open Run > Edit Configurations...
# Add a new PHP Web Application and name it whatever.
# In the Server dropdown, select the server you just created.
# The start URL should be whatever URL you want to run the debugger on.

```